const router = require('express').Router()

const teacherController = require('../controller/teacherController')
const alunoController = require('../controller/alunoController')
const JSONPlaceholderController = require('../controller/JSONPlaceholderController')

router.get('/', teacherController.getTeachers);
router.post('/cadastroaluno', alunoController.postAlunos);
router.put('/editarAluno', alunoController.updateAluno);
router.delete('/deletarAluno/:id', alunoController.deleteAluno);

router.get('/external/', JSONPlaceholderController.getUsers);
router.get('/external/io', JSONPlaceholderController.getUsersWebsiteIO);
router.get('/external/com', JSONPlaceholderController.getUsersWebsiteCOM);
router.get('/external/net', JSONPlaceholderController.getUsersWebsiteNET);
router.get('/external/filter', JSONPlaceholderController.getCountDomain);


module.exports = router