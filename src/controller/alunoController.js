module.exports = class alunoController{


    //Listar Alunos
    static async postAlunos(req, res) {
        const { nome, idade, profissao, cursoMatriculado } = req.body;
        console.log(`nome: ${nome}, idade: ${idade}, idade: ${profissao}: , Curso Matriculado: ${cursoMatriculado};`)
    res.status(200).json({message: "aluno criado"})
    }

    static async updateAluno(req, res) {
        const { nome, idade, profissao, cursoMatriculado} = req.body;
        
        if(nome === "" || idade === 0 || profissao === "" || cursoMatriculado === ""){
            res.status(200).json({message: "dados invalidos"})
        } else{
            res.status(200).json({message: "aluno atualizado", dados: `nome: ${nome}, idade: ${idade}, profissão: ${profissao}, curso matriculado: ${cursoMatriculado}`})
        }
        
    }

    static async deleteAluno(req, res){
        
        if(req.params.id == 'true'){
            res.status(200).json({message: "Aluno deletado"})
        } else{
            res.status(200).json({message: "Aluno não deletado: id não corresponde"})
        }
    }
}